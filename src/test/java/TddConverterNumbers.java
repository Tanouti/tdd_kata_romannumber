import org.junit.Assert;
import org.junit.Test;

public class TddConverterNumbers {


    @Test
    public void Should_1_given_I(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(1);
        Assert.assertEquals("I", RomanNumber);
    }

    @Test
    public void Should_2_given_II(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(2);
        Assert.assertEquals("II", RomanNumber);
    }

    @Test
    public void Should_3_given_III(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(3);
        Assert.assertEquals("III", RomanNumber);
    }


    @Test
    public void Should_4_given_IV(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(4);
        Assert.assertEquals("IV", RomanNumber);
    }

    @Test
    public void Should_5_given_V(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(5);
        Assert.assertEquals("V", RomanNumber);
    }
    @Test
    public void Should_6_given_VI(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(6);
        Assert.assertEquals("VI", RomanNumber);
    }
    @Test
    public void Should_9_given_IX(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(9);
        Assert.assertEquals("IX", RomanNumber);
    }
    @Test
    public void Should_10_given_9(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(10);
        Assert.assertEquals("X", RomanNumber);
    }
    @Test
    public void Should_11_given_XI(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(11);
        Assert.assertEquals("XI", RomanNumber);
    }
    @Test
    public void Should_14_given_XIV(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(14);
        Assert.assertEquals("XIV", RomanNumber);
    }
    @Test
    public void Should_15_given_XV(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(15);
        Assert.assertEquals("XV", RomanNumber);
    }
    @Test
    public void Should_16_given_XVI(){

        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(16);
        Assert.assertEquals("XVI", RomanNumber);
    }
    @Test
    public void Should_19_given_XIX(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(19);
        Assert.assertEquals("XIX", RomanNumber);
    }
    @Test
    public void Should_20_given_XX(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(20);
        Assert.assertEquals("XX", RomanNumber);
    }
    @Test
    public void Should_25_given_XXV(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(25);
        Assert.assertEquals("XXV", RomanNumber);
    }
    @Test
    public void Should_29_given_XXIX(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(29);
        Assert.assertEquals("XXIX", RomanNumber);
    }
    @Test
    public void Should_40_given_XL(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(40);
        Assert.assertEquals("XL", RomanNumber);
    }
    @Test
    public void Should_50_given_L(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(50);
        Assert.assertEquals("L", RomanNumber);
    }
    @Test
    public void Should_60_given_LX(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(60);
        Assert.assertEquals("LX", RomanNumber);
    }
    @Test
    public void Should_90_given_XC(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(90);
        Assert.assertEquals("XC", RomanNumber);
    }
    @Test
    public void Should_91_given_XC(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(91);
        Assert.assertEquals("XCI", RomanNumber);
    }
    @Test
    public void Should_100_given_C(){
        ConverterNumber converter = new ConverterNumber();
        String RomanNumber = converter.convert(100);
        Assert.assertEquals("C", RomanNumber);
    }
}
