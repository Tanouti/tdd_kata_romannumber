import java.util.*;

public class ConverterNumber {
    public static Map romanNumbers = new HashMap<Integer,String>(){
        {
            put(1,"I");
            put(4,"IV");
            put(5,"V");
            put(9,"IX");
            put(10,"X");
            put(40,"XL");
            put(50,"L");
            put(90,"XC");
            put(100,"C");
        }
    };

    public String convert(int arabicNumber){
        String romanNumber ="";

        while(arabicNumber>0)
        {
                int rightIndice =getTheRightValue(arabicNumber);
                romanNumber += romanNumbers.get(rightIndice);
                arabicNumber -=rightIndice;
        }
        return romanNumber;
    }

    public int getTheRightValue(int number){
        Set<Integer> romanNumbersSorted = new TreeSet(romanNumbers.keySet());
        Iterator iter = romanNumbersSorted.iterator();
        int result=1;
        while(iter.hasNext())
        {
            int current = (Integer) iter.next();
            if(current<=number)
            {
                result = current;
            }
        }
        return result;
    }

}
